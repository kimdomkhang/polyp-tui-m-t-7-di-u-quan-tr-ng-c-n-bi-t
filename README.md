# Polyp túi mật 7 điều quan trọng cần biết

Polyp túi mật là một bệnh đường mật khá phổ biến, đa số là lành tính, chỉ có một số ít (khoảng 8%) có thể tiến triển thành ung thư. Hiểu rõ về nguyên nhân, triệu chứng, cách điều trị polyp túi mật sẽ giúp bạn hạn chế được những yếu tố nguy cơ bệnh